class InventoryService:


    def __init__(self, inventory_repo):
        self.inventory_repo = inventory_repo


    def add_inventory(self, inventory):
        pass

    def get_inventory_for_date(self, hotel_id, date):
        pass

    def get_availability_for_date(self, hotel_id, date):
        pass

    def get_throttled_availability_for_date(self, hotel_id, date, channel=None):
        pass

    def block_room(self, hotel_id, date):
        pass
