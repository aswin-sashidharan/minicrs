class Hotel:
    def __init__(self, name, rooms=None):
        self.name = name
        self.rooms = [] if rooms is None else rooms


class Room:
    def __init__(self, room_no):
        self.room_no = room_no


class Inventory:
    def __init__(self, hotel, date, availability):
        self.hotel = hotel
        self.date = date
        self.availability = availability
