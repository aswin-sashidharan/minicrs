import uuid


class CatalogRepository:

    def __init__(self):
        self.catalog = {}

    def save(self, hotel):

        hotel_id = uuid.uuid4()
        hotel.hotel_id = hotel_id

        self.catalog[hotel_id] = hotel
        return hotel_id


    def get(self, hotel_id):
        return self.catalog[hotel_id] #TODO throw not found

    def clear(self):
        self.catalog = {}

class InventoryRepository:

    def __init__(self):
        self.inventory = {}

    def save(self, inventory):
        self.inventory[inventory.hotel.hotel_id, inventory.date] = inventory

    def get(self, hotel_id, date):
        return self.inventory[hotel_id, date]

    def clear(self):
        self.inventory = {}
